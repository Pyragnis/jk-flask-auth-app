pipeline {
    agent any

    environment {
        DOCKER_IMAGE = 'pyragnis91/jk_flask_auth_app'
        IMAGE_TAG = 'latest'
        REGISTRY_CREDENTIALS_ID = 'dockerhub-login' 
        HEROKU_APP_NAME = 'flask20'
    }

    stages {
        stage('Build image') {
            steps {
                script {
                    // Building the Docker image
                    bat "docker build -t ${DOCKER_IMAGE}:${IMAGE_TAG} ."
                }
            }
        }

        stage('Push image to Docker Hub') {
            steps {
                script {
                    // Pushing the Docker image to Docker Hub
                    withCredentials([usernamePassword(credentialsId: REGISTRY_CREDENTIALS_ID, usernameVariable: 'DOCKERHUB_USER', passwordVariable: 'DOCKERHUB_PASS')]) {
                        docker.withRegistry('https://docker.io', 'dockerhub-login') {
                            def dockerImage = docker.image("${DOCKER_IMAGE}:${IMAGE_TAG}")
                            dockerImage.push()
                        }
                    }
                }
            }
        }

       stage('Deploy to Heroku') {
    steps {
        script {
            // Deploying the Docker image to Heroku
            withCredentials([string(credentialsId: 'heroku_api_key', variable: 'HEROKU_API_KEY')]) {
                bat """
                docker tag ${DOCKER_IMAGE}:${IMAGE_TAG} registry.heroku.com/${HEROKU_APP_NAME}/web
                echo $HEROKU_API_KEY | docker login --username=_ --password-stdin registry.heroku.com
                docker push registry.heroku.com/${HEROKU_APP_NAME}/web
                heroku container:release web --app ${HEROKU_APP_NAME}
                """
            }
        }
    }
}



        stage('Check Deployment') {
            steps {
                script {
                    // Checking if the deployed application is live and accessible
                    bat "curl -s https://${HEROKU_APP_NAME}.herokuapp.com"
                }
            }
        }
    }

    post {
        always {
            // Sending build completion email
            emailext(
                to: 'enzosabbatorsi@outlook.fr',
                subject: "Build ${currentBuild.fullDisplayName} completed",
                body: """
                <html><body>
                <p>The build of <strong>${env.JOB_NAME}</strong> build number <strong>${env.BUILD_NUMBER}</strong> was completed.</p>
                <p>Status: <strong>${currentBuild.currentResult}</strong></p>
                <p>View build details at <a href='${BUILD_URL}'>${BUILD_URL}</a>.</p>
                </body></html>""",
                mimeType: 'text/html'
            )
        }
        success {
            echo 'Build and deployment were successful!'
        }
        failure {
            echo 'Build or deployment failed.'
        }
    }
}
